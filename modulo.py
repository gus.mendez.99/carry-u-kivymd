from datetime import datetime

def registrarUsuario(db, usuario, rol) :#Rol Usuario: 1, Asistente: 2 
    exito = False
    coleccion = {}
    if(rol == 1):
        coleccion = db["usuarios"]
    elif(rol == 2):
        coleccion = db["asistentes"]
    
    if(buscarUsuario(db, usuario["usuario"]) != 1): #Verifica si no hay usuarios existentes con ese nick
        coleccion.insert(usuario)
        exito = True
    #print("EXITO:: " + exito)
    return exito
  
def buscarUsuario(db, nick):
  coleccion1 = db["usuarios"]
  coleccion2 = db["asistentes"]
  
  if(coleccion1.find({"usuario": nick}).count() > 0 or coleccion2.find({"usuario": nick}).count() > 0):
    return 1 #Retorna 1 porque verifica que hay un usuario o asistente
              #Con ese nick 
  return 0


def obtenerAsistente(db, nick):
  return db.asistentes.find_one({"usuario": nick})

def obtenerUsuario(db, nick):
  return db.usuarios.find_one({"usuario": nick})



  
def login(db, user, contra):
    #uno = "error en login"
    #dos = "login correcto Usuario"
    #tres = "login correcto Asistente"
    r = 0
    if(db.usuarios.find({"usuario":user}).count() > 0 and contra == db.usuarios.find_one({"usuario":user})["contrasena"]):
    	r = 2          
    elif (db.asistentes.find({"usuario":user}).count() > 0 and contra == db.asistentes.find_one({"usuario":user})["contrasena"]):
        r = 3         
    else:
       	r = 1
        
    return r

  
def agregarAsistencia(db, asistencia):
    exito = False
    coleccion = db.asistencias
    if(asistencia["usuario"] != "" and asistencia["ubicacion"] != ""):
        coleccion.insert(asistencia)
        exito = True
    return exito
  
def buscarAsistencia(db, _id, usuario, rol):
    if(_id != 0): #Existe un id
        return db.asistencias.find_one({"_id": _id}) #Retorna una asistencia {"asistente": "juanito", "hI": "15:00"}
    else:
        if(rol == 1): #Usuario
            return db.asistencias.find({"usuario": usuario}).sort("_id",-1).limit(1)  #Retornar una lista con una sola asistencia {{"asistente": "juanito", "hI": "15:00"}}
        elif(rol == 2): #Asistente
            return db.asistencias.find({"asistente": usuario}).sort("_id",-1).limit(1) #Retorna una asistencia pero segun el asistente


def buscarAsistenciasPorEstado(db, estado):
  if(estado != ""): #Existe un id
    return db.asistencias.find({"estado": estado}) #Retorna una asistencia {"asistente": "juanito", "hI": "15:00"}


def obtenerIdFromIndexCursor(db, estado, index):
  if(estado != ""): #Existe un id
    return db.asistencias.find({"estado": estado}) [index]#Retorna una asistencia {"asistente": "juanito", "hI": "15:00"}

  
def modificarAsistencia(db, cambios, _id, usuario, rol):
    exito = False
    coleccion = db.asistencias
    if(buscarAsistencia(db, _id, usuario, rol)):
        coleccion.update({"_id":_id},{"$set":cambios})
        exito = True
    return exito
  

def verificarEstadoAsistencia(db, _id, usuario, rol):
  return buscarAsistencia(db, _id, usuario, rol)["estado"]
  
  
def imprimirAsistencias(asistencias):
  index = 0
  s = ""
  for asistencia in asistencias:
    s = s + (str(index + 1) + " - Usuario: " + asistencia["usuario"] + ", Ubicacion: " + asistencia["ubicacion"] + "\n")
    index += 1
  return s
  
#Utils - Independientes de MongoDB
def calcularHorasTrabajadas(fecha, horaInicio, horaFin):
    date_format = "%d-%m-%Y %H:%M:%S"
    #convert string to actual date and time
    time1  = datetime.strptime(fecha + " " + horaInicio, date_format)
    time2  = datetime.strptime(fecha + " " + horaFin, date_format)

    #find the difference between two dates
    diff = time2 - time1

    days = diff.days    

    #print overall hours
    days_to_hours = days * 24
    diff_btw_two_times = (diff.seconds) / 3600
    overall_hours = days_to_hours + diff_btw_two_times
    return float(overall_hours)
  

def opcionEnRango(x, a, b):#Funcion para validar si un numero x esta dentro de un rango establecido.
    if (x >= a and x <= b) or (x <= a and x >= b):#Si x se encuentra dentro del rango de a y b
        return True#Retornara True
    else:
        return False#De lo contrario retonrara falso

def validarNumero(num): #Funcion para validar que el dato ingresado por el usuario sea un numero
    try:#Que verifique si:
        int(num)#Se divide el dato ingresado entre uno para verificar que sea un numero, si lo es, se duevuelve True
        return True
    except ValueError:#La excpecion sera un error de valor y retornara falso
        return False
      
      #Verifica si existe coleccion y si no, la crea
  #def existente (nombre_coleccion):
   # exito = "Coleccion creada con exito"
    #if (db.find[nombre_coleccion]).count()>0:
     #   return True
    #else:
     #   conexion = pymongo.MongoClient()
      #  use["agregar nombre de la base de datos nueva"]
       # return exito
        
