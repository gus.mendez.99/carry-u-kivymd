# -*- coding: utf-8 -*-
from kivy.app import App
from kivy.lang import Builder
from kivy.metrics import dp
from kivy.properties import ObjectProperty, StringProperty
from kivy.core.window import Window
from kivy.uix.image import Image

#from jnius import cast
#from jnius import autoclass


from kivymd.bottomsheet import MDListBottomSheet, MDGridBottomSheet
from kivymd.button import MDIconButton
from kivymd.date_picker import MDDatePicker
from kivymd.dialog import MDDialog
from kivymd.label import MDLabel
from kivymd.list import ILeftBody, ILeftBodyTouch, IRightBodyTouch, BaseListItem
from kivymd.material_resources import DEVICE_TYPE
from kivymd.navigationdrawer import MDNavigationDrawer, NavigationDrawerHeaderBase
from kivymd.selectioncontrols import MDCheckbox
from kivymd.snackbar import Snackbar
from kivymd.theming import ThemeManager
from kivymd.time_picker import MDTimePicker

from kivymd.list import BaseListItem, TwoLineListItem, TwoLineAvatarIconListItem

#Importaciones del Proyecto
from time import strftime, sleep
from modulo import *
import pymongo
from bson import ObjectId
from datetime import datetime

from pymongo import MongoClient
from pymongo.errors import ConnectionFailure


conexion = MongoClient("mongodb://carryuuvg:carryuuvg@ds261479.mlab.com:61479/carry_u")
db = conexion.carry_u


main_widget_kv = '''
#:import Toolbar kivymd.toolbar.Toolbar
#:import ThemeManager kivymd.theming.ThemeManager
#:import MDNavigationDrawer kivymd.navigationdrawer.MDNavigationDrawer
#:import NavigationLayout kivymd.navigationdrawer.NavigationLayout
#:import NavigationDrawerDivider kivymd.navigationdrawer.NavigationDrawerDivider
#:import NavigationDrawerToolbar kivymd.navigationdrawer.NavigationDrawerToolbar
#:import NavigationDrawerSubheader kivymd.navigationdrawer.NavigationDrawerSubheader
#:import MDCheckbox kivymd.selectioncontrols.MDCheckbox
#:import MDSwitch kivymd.selectioncontrols.MDSwitch
#:import MDList kivymd.list.MDList
#:import OneLineListItem kivymd.list.OneLineListItem
#:import TwoLineListItem kivymd.list.TwoLineListItem
#:import ThreeLineListItem kivymd.list.ThreeLineListItem
#:import OneLineAvatarListItem kivymd.list.OneLineAvatarListItem
#:import OneLineIconListItem kivymd.list.OneLineIconListItem
#:import OneLineAvatarIconListItem kivymd.list.OneLineAvatarIconListItem
#:import MDTextField kivymd.textfields.MDTextField
#:import MDSpinner kivymd.spinner.MDSpinner
#:import MDCard kivymd.card.MDCard
#:import MDSeparator kivymd.card.MDSeparator
#:import MDDropdownMenu kivymd.menu.MDDropdownMenu
#:import get_color_from_hex kivy.utils.get_color_from_hex
#:import colors kivymd.color_definitions.colors
#:import SmartTile kivymd.grid.SmartTile
#:import MDSlider kivymd.slider.MDSlider
#:import MDTabbedPanel kivymd.tabs.MDTabbedPanel
#:import MDTab kivymd.tabs.MDTab
#:import MDProgressBar kivymd.progressbar.MDProgressBar
#:import MDAccordion kivymd.accordion.MDAccordion
#:import MDAccordionItem kivymd.accordion.MDAccordionItem
#:import MDAccordionSubItem kivymd.accordion.MDAccordionSubItem
#:import MDThemePicker kivymd.theme_picker.MDThemePicker
#:import MDBottomNavigation kivymd.tabs.MDBottomNavigation
#:import MDBottomNavigationItem kivymd.tabs.MDBottomNavigationItem





NavigationLayout:
    id: nav_layout
    MDNavigationDrawer:
        id: nav_drawer
        NavigationDrawerToolbar:
            title: "Carry U Menu"
        NavigationDrawerIconButton:
            icon: 'checkbox-blank-circle'
            text: "Modificar perfil"
            on_release: app.root.ids.scr_mngr.current = 'accordion'
        NavigationDrawerIconButton:
            icon: 'checkbox-blank-circle'
            text: "Cerrar Sesion"
            on_release: app.root.ids.scr_mngr.current = 'bottom_navigation'
        NavigationDrawerIconButton:
            icon: 'checkbox-blank-circle'
            text: "Salir"
            on_release: app.root.ids.scr_mngr.current = 'bottomsheet'

    BoxLayout:
        orientation: 'vertical'
        ScreenManager:
            id: scr_mngr            
            Screen:                
                name: 'home'
                BoxLayout:
                    orientation: 'vertical'
                    Toolbar:
                        id: toolbar
                        title: 'Carry U App'
                        md_bg_color: app.theme_cls.primary_color
                        background_palette: 'Primary'
                        background_hue: '500'
                        #left_action_items: [['menu', lambda x: app.root.toggle_nav_drawer()]]
                        #right_action_items: [['dots-vertical', lambda x: app.root.toggle_nav_drawer()]]

                    BoxLayout:
                        orientation: 'vertical'
                        #size_hint_y: None
                        #height: dp(1000)
                        MDLabel:
                            font_style: 'Title'
                            theme_text_color: 'Primary'
                            text: "Elige la opcion deseada"
                            halign: 'center'
                        MDFlatButton:
                            text: 'Login'
                            pos_hint: {'center_x': 0.5, 'center_y': 0.6}
                            on_release: app.root.ids.scr_mngr.current = 'login'
                        MDRaisedButton:
                            text: "Registro Usuario"
                            elevation_normal: 2
                            opposite_colors: True
                            pos_hint: {'center_x': 0.5, 'center_y': 0.4}
                            on_release: app.root.ids.scr_mngr.current = 'registro_usuario'
                        
                        MDRaisedButton:
                            text: "Registro Asistente"
                            elevation_normal: 2
                            opposite_colors: True
                            pos_hint: {'center_x': 0.5, 'center_y': 0.4}
                            on_release: app.root.ids.scr_mngr.current = 'registro_asistente'
            

            Screen:
                name: 'login'
                BoxLayout:
                    orientation: 'vertical'
                    Toolbar:
                        id: toolbar
                        title: 'Carry U App'
                        md_bg_color: app.theme_cls.primary_color
                        background_palette: 'Primary'
                        background_hue: '500'
                        left_action_items: [['arrow-left', lambda x: app.navigate_to_screen('home')]]
                        #right_action_items: [['dots-vertical', lambda x: app.root.toggle_nav_drawer()]]

                    ScrollView:
                        BoxLayout:
                            orientation: 'vertical'
                            size_hint_y: None
                            height: self.minimum_height
                            padding: dp(48)
                            spacing: 10
                            
                            MDTextField:
                                id: usuarioTextfield
                                hint_text: "Usuario"
                                #required: True
                            MDTextField:
                                id: passTextfield
                                hint_text: "Contraseña"
                                #required: True
                                password: True

                            MDRaisedButton:
                                text: "Entrar"
                                elevation_normal: 2
                                opposite_colors: True
                                pos_hint: {'center_x': 0.5, 'center_y': 0.4}
                                on_release: app.loguearse()


            Screen:
                name: 'registro_usuario'
                BoxLayout:
                    orientation: 'vertical'
                    Toolbar:
                        id: toolbar
                        title: 'Registro Usuario'
                        md_bg_color: app.theme_cls.primary_color
                        background_palette: 'Primary'
                        background_hue: '500'
                        left_action_items: [['arrow-left', lambda x: app.navigate_to_screen('home')]]

                    ScrollView:
                        BoxLayout:
                            orientation: 'vertical'
                            size_hint_y: None
                            height: self.minimum_height
                            padding: dp(48)
                            spacing: 10
                            
                            MDTextField:
                                id: nombreTextField
                                hint_text: "Nombre"
                                #required: True
                            MDTextField:
                                id: apellidoTextField
                                hint_text: "Apellido"
                                #required: True                                
                            MDTextField:
                                id: usuarioTextField
                                hint_text: "Usuario"
                                #required: True
                            MDTextField:
                                id: contrasenaTextField
                                hint_text: "Contraseña"
                                #required: True
                                password: True
                            
                            #Fecha
                            BoxLayout:
                                orientation: 'horizontal'
                                size_hint: None, None
                                spacing: 8                            
                                MDRaisedButton:
                                    text: "Fecha de nac."
                                    size_hint: None, None
                                    size: 4 * dp(48), dp(48)
                                    pos_hint: {'center_x': 0.5, 'center_y': 0.5}
                                    opposite_colors: True
                                    on_release: app.show_example_date_picker()
                                MDLabel:
                                    id: date_picker_label
                                    theme_text_color: 'Primary'
                                    size_hint: None, None
                                    size: dp(48)*3, dp(48)
                                    pos_hint: {'center_x': 0.5, 'center_y': 0.5}

                            MDLabel:
                                font_style: 'Body1'
                                theme_text_color: 'Primary'
                                text: "Sexo (F/M):"
                            BoxLayout:
                                orientation: 'vertical'
                                #halign: 
                                pos_hint: {'center_x': 1.2, 'center_y': 0}
                                height: "300dp"
                                BoxLayout:
                                    orientation: 'horizontal'                          
                                    MDCheckbox:
                                        id:           checkSexoF
                                        group:        'sexo'
                                        size_hint:    None, None
                                        size:        dp(30), dp(30)
                                    MDCheckbox:
                                        id:            checkSexoM
                                        group:        'sexo'
                                        size_hint:    None, None
                                        size:        dp(30), dp(30)


                            MDTextField:
                                id: dpiTextField
                                hint_text: "DPI"
                                #required: True
                            MDTextField:
                                id: numeroTextField
                                hint_text: "Numero"
                                #required: True
                            MDTextField:
                                id: correoTextField
                                hint_text: "Correo"
                                #required: True
                            
                            MDLabel:
                                font_style: 'Body1'
                                width: dp(48)
                                theme_text_color: 'Primary'
                                text: "Necesitare ayuda para:"

                            BoxLayout:
                                size_hint: None, None                              
                                orientation: 'horizontal' 
                                MDLabel:
                                    font_style: 'Body1'
                                    width: dp(48)
                                    theme_text_color: 'Primary'
                                    text: "Cargar cosas:"

                                MDSwitch:
                                    id: switchCargar
                                    size_hint:    None, None
                                    size:        dp(24), dp(24)
                                    pos_hint:    {'center_x': 0.75, 'center_y': 0.5}
                                    _active:        False
                                
                            BoxLayout:
                                orientation: 'horizontal' 
                                size_hint: None, None
                                MDLabel:
                                    font_style: 'Body1'
                                    width: dp(48)
                                    theme_text_color: 'Primary'
                                    text: "Movilizarme en lugares:"

                                MDSwitch:
                                    id: switchLlevarSilla
                                    size_hint:    None, None
                                    size:        dp(24), dp(24)
                                    pos_hint:    {'center_x': 0.75, 'center_y': 0.5}
                                    _active:        False

                            BoxLayout:
                                orientation: 'horizontal' 
                                size_hint: None, None
                                MDLabel:
                                    font_style: 'Body1'
                                    width: dp(48)
                                    theme_text_color: 'Primary'
                                    text: "Ayuda con silla/muletas:"

                                MDSwitch:
                                    id: switchMovilizar
                                    size_hint:    None, None
                                    size:        dp(24), dp(24)
                                    pos_hint:    {'center_x': 0.75, 'center_y': 0.5}
                                    _active:        False
                            

                            MDRaisedButton:
                                text: "Registrarse"
                                elevation_normal: 2
                                opposite_colors: True
                                pos_hint: {'center_x': 0.5, 'center_y': 0.4}
                                on_release: app.registrarse('usuario')


            #REGISTRO ASISTENTE
            Screen:
                name: 'registro_asistente'
                BoxLayout:
                    orientation: 'vertical'
                    Toolbar:
                        id: toolbar
                        title: 'Registro Asistente'
                        md_bg_color: app.theme_cls.primary_color
                        background_palette: 'Primary'
                        background_hue: '500'
                        left_action_items: [['arrow-left', lambda x: app.navigate_to_screen('home')]]

                    ScrollView:
                        BoxLayout:
                            orientation: 'vertical'
                            size_hint_y: None
                            height: self.minimum_height
                            padding: dp(48)
                            spacing: 10
                            
                            MDTextField:
                                id: nombreTextField
                                hint_text: "Nombre"
                                #required: True
                            MDTextField:
                                id: apellidoTextField
                                hint_text: "Apellido"
                                #required: True                                
                            MDTextField:
                                id: usuarioTextField
                                hint_text: "Usuario"
                                #required: True
                            MDTextField:
                                id: contrasenaTextField
                                hint_text: "Contraseña"
                                #required: True
                                password: True
                            
                            #Fecha
                            BoxLayout:
                                orientation: 'horizontal'
                                size_hint: None, None
                                spacing: 8                            
                                MDRaisedButton:
                                    text: "Fecha de nac."
                                    size_hint: None, None
                                    size: 4 * dp(48), dp(48)
                                    pos_hint: {'center_x': 0.5, 'center_y': 0.5}
                                    opposite_colors: True
                                    on_release: app.show_example_date_picker()
                                MDLabel:
                                    id: date_picker_label
                                    theme_text_color: 'Primary'
                                    size_hint: None, None
                                    size: dp(48)*3, dp(48)
                                    pos_hint: {'center_x': 0.5, 'center_y': 0.5}

                            MDLabel:
                                font_style: 'Body1'
                                theme_text_color: 'Primary'
                                text: "Sexo (F/M):"
                            BoxLayout:
                                orientation: 'vertical'
                                #halign: 
                                pos_hint: {'center_x': 1.2, 'center_y': 0}
                                height: "300dp"
                                BoxLayout:
                                    orientation: 'horizontal'                          
                                    MDCheckbox:
                                        id:           checkSexoF
                                        group:        'sexo'
                                        size_hint:    None, None
                                        size:        dp(30), dp(30)
                                    MDCheckbox:
                                        id:            checkSexoM
                                        group:        'sexo'
                                        size_hint:    None, None
                                        size:        dp(30), dp(30)


                            MDTextField:
                                id: dpiTextField
                                hint_text: "DPI"
                                #required: True
                            MDTextField:
                                id: numeroTextField
                                hint_text: "Numero"
                                #required: True
                            MDTextField:
                                id: correoTextField
                                hint_text: "Correo"
                                #required: True
                            
                            MDLabel:
                                font_style: 'Body1'
                                width: dp(48)
                                theme_text_color: 'Primary'
                                text: "Puedo ayudar a:"

                            BoxLayout:
                                size_hint: None, None                              
                                orientation: 'horizontal' 
                                MDLabel:
                                    font_style: 'Body1'
                                    width: dp(48)
                                    theme_text_color: 'Primary'
                                    text: "Cargar cosas:"

                                MDSwitch:
                                    id: switchCargar
                                    size_hint:    None, None
                                    size:        dp(24), dp(24)
                                    pos_hint:    {'center_x': 0.75, 'center_y': 0.5}
                                    _active:        False
                                
                            BoxLayout:
                                orientation: 'horizontal' 
                                size_hint: None, None
                                MDLabel:
                                    font_style: 'Body1'
                                    width: dp(48)
                                    theme_text_color: 'Primary'
                                    text: "Movilizar en lugares:"

                                MDSwitch:
                                    id: switchLlevarSilla
                                    size_hint:    None, None
                                    size:        dp(24), dp(24)
                                    pos_hint:    {'center_x': 0.75, 'center_y': 0.5}
                                    _active:        False

                            BoxLayout:
                                orientation: 'horizontal' 
                                size_hint: None, None
                                MDLabel:
                                    font_style: 'Body1'
                                    width: dp(48)
                                    theme_text_color: 'Primary'
                                    text: "Ayudar con silla/muletas:"

                                MDSwitch:
                                    id: switchMovilizar
                                    size_hint:    None, None
                                    size:        dp(24), dp(24)
                                    pos_hint:    {'center_x': 0.75, 'center_y': 0.5}
                                    _active:        False
                            

                            MDRaisedButton:
                                text: "Registrarse"
                                elevation_normal: 2
                                opposite_colors: True
                                pos_hint: {'center_x': 0.5, 'center_y': 0.4}
                                on_release: app.registrarse('asistente')


            Screen:                
                name: 'home_usuario'
                BoxLayout:
                    orientation: 'vertical'
                    Toolbar:
                        id: toolbar
                        title: 'Carry U App'
                        md_bg_color: app.theme_cls.primary_color
                        background_palette: 'Primary'
                        background_hue: '500'
                        left_action_items: [['menu', lambda x: app.root.toggle_nav_drawer()]]
                        right_action_items: [['dots-vertical', lambda x: app.root.toggle_nav_drawer()]]
                    ScrollView:
                        BoxLayout:
                            orientation: 'vertical'
                            size_hint_y: None
                            height: self.minimum_height
                            padding: dp(48)
                            spacing: 30
                            MDLabel:
                                font_style: 'Body1'
                                width: dp(48)
                                theme_text_color: 'Primary'
                                text: "¿Dónde necesitas la asistencia?"
                            MDTextField:
                                id: lugar_asistenciaTextField
                                hint_text: "Ingresa aquí el lugar"
                                #required: True
                            MDRaisedButton:
                                text: "Solicitar Asistencia"
                                elevation_normal: 2
                                opposite_colors: True
                                pos_hint: {'center_x': 0.5, 'center_y': 0.4}
                                on_release: app.solicitar_asistencia()
            #----------------------------------------------------------------------------------------------
            Screen:                
                name: 'en_espera_usuario'
                on_enter: app.Search_Asistencia()
                BoxLayout:
                    orientation: 'vertical'
                    Toolbar:
                        id: toolbar
                        title: 'Buscando Asistentes cercanos'
                        md_bg_color: app.theme_cls.primary_color
                        background_palette: 'Primary'
                        background_hue: '500'
                        left_action_items: [['menu', lambda x: app.root.toggle_nav_drawer()]]
                        right_action_items: [['dots-vertical', lambda x: app.root.toggle_nav_drawer()]]
                    ScrollView:
                        BoxLayout:
                            orientation: 'vertical'
                            size_hint_y: None
                            height: self.minimum_height
                            padding: dp(48)
                            spacing: 40
                            MDLabel:
                                font_style: 'Title'
                                width: dp(48)
                                theme_text_color: 'Primary'
                                text: "Conectándote con asistentes cercanos..."
                            MDLabel:
                                font_style: 'Body1'
                                width: dp(48)
                                theme_text_color: 'Primary'
                                text: "Te avisaremos cuando algún asistente confirme"
                            MDSpinner:
                                id: spinner1
                                size_hint: None, None
                                size: dp(46), dp(46)
                                pos_hint: {'center_x': 0.5, 'center_y': 0.5}
                                active: True
            #-------------------------------------------------------------------------------------------
            #DETALLE DEL ASISTENTE PARA QUE EL USUARIO LO VISUALICE
            Screen:
                name: 'detalle_asistente_usuario'
                on_enter: app.en_espera_llegada()
                Widget: 
                    id: title_line 

                    canvas: 
                        Color: 
                            rgba: app.theme_cls.primary_color 
                        Rectangle: 
                            size: self.size 
                            pos: self.pos 

                    size_hint_y: None 
                    height: root.height * 30 / 100  # 30% от высоты экрана 
                    pos: 0, root.height - self.size[1] 

                Toolbar:
                    id: toolbar
                    title: 'Perfil del Asistente' 
                    md_bg_color: app.theme_cls.primary_color
                    background_palette: 'Primary'
                    background_hue: '500'
                    #left_action_items: [['arrow-left', lambda x: app.navigate_to_screen('home_usuario')]]
                    #right_action_items: [['dots-vertical', lambda x: app.root.toggle_nav_drawer()]]
                    pos: 0, root.height - self.size[1] 

                Widget: 
                    canvas: 
                        Ellipse: 
                            pos: self.pos 
                            size: 125, 125 
                            source: 'assets/user.png' 
                    pos: (root.width / 2) - 60, root.height * 61 / 100 

                BoxLayout: 
                    orientation: 'vertical' 
                    size_hint: 1, None 
                    #height: 50
                    spacing: 35
                    pos: self.pos[0], root.height * 28 / 125 

                    MDLabel: 
                        id: nombreasistenteTextField 
                        font_style: 'Headline' 
                        theme_text_color: 'Primary' 
                        #color: app.data.text_color 
                        text: ''
                        halign: 'center' 
                    MDLabel: 
                        id: telefonoasistenteTextField 
                        font_style: 'Subhead' 
                        theme_text_color: 'Primary' 
                        #color: app.data.text_color 
                        text: ''
                        halign: 'center' 
                    MDLabel: 
                        id: dpiTextField 
                        font_style: 'Subhead' 
                        theme_text_color: 'Primary' 
                        #color: app.data.text_color 
                        text: '' 
                        halign: 'center' 
                    MDLabel: 
                        id: sexoTextField 
                        font_style: 'Subhead' 
                        theme_text_color: 'Primary' 
                        #color: app.data.text_color 
                        text: '' 
                        halign: 'center' 
                    MDLabel: 
                        id: aptitud_CargarTextField 
                        font_style: 'Subhead' 
                        theme_text_color: 'Primary' 
                        #color: app.data.text_color 
                        text: '' 
                        halign: 'center'    
                    MDLabel: 
                        id: aptitud_MovilizarTextField 
                        font_style: 'Subhead' 
                        theme_text_color: 'Primary' 
                        #color: app.data.text_color 
                        text: '' 
                        halign: 'center'  
                    MDLabel: 
                        id: aptitud_MoverSillaTextField 
                        font_style: 'Subhead' 
                        theme_text_color: 'Primary' 
                        #color: app.data.text_color 
                        text: '' 
                        halign: 'center'     
            #________-----------------------------------------------------------------------------------------       
            #USUARIO EN CURSO
           
            Screen:
                name: 'en_curso_usuario'
                on_enter: app.espera_finalizacion()
                Widget: 
                    id: title_line 

                    canvas: 
                        Color: 
                            rgba: app.theme_cls.primary_color 
                        Rectangle: 
                            size: self.size 
                            pos: self.pos 

                    size_hint_y: None 
                    height: root.height * 30 / 100  # 30% от высоты экрана 
                    pos: 0, root.height - self.size[1] 

                Toolbar:
                    id: toolbar
                    title: 'En Curso: Usuario'
                    md_bg_color: app.theme_cls.primary_color
                    background_palette: 'Primary'
                    background_hue: '500'
                    left_action_items: [['arrow-left', lambda x: app.navigate_to_screen('home_usuario')]]
                    #right_action_items: [['dots-vertical', lambda x: app.root.toggle_nav_drawer()]]
                    pos: 0, root.height - self.size[1] 

                Widget: 
                    canvas: 
                        Ellipse: 
                            pos: self.pos 
                            size: 125, 125 
                            source: 'assets/wait.png' 
                    pos: (root.width / 2) - 60, root.height * 61 / 100 

                BoxLayout: 
                    orientation: 'vertical' 
                    size_hint: 1, None
                    #height: 50
                    spacing: 30
                    pos: self.pos[0], root.height * 20 / 100
                    MDLabel: 
                        id: info_servicio_activo
                        font_style: 'Headline' 
                        theme_text_color: 'Primary' 
                        #color: app.data.text_color 
                        text: 'Tu servicio está activo' 
                        halign: 'center' 
                    MDLabel: 
                        id: nombreasistenteTextField1 
                        font_style: 'Subhead' 
                        theme_text_color: 'Primary' 
                        #color: app.data.text_color 
                        text: '' 
                        halign: 'center'    
                    MDLabel: 
                        id: telefonoasistenteTextField1 
                        font_style: 'Subhead' 
                        theme_text_color: 'Primary' 
                        #color: app.data.text_color 
                        text: '' 
                        halign: 'center'  
                    MDLabel: 
                        id: dpiTextField1
                        font_style: 'Subhead' 
                        theme_text_color: 'Primary' 
                        #color: app.data.text_color 
                        text: '' 
                        halign: 'center'
                    MDLabel: 
                        id: ubicacionTextField1
                        font_style: 'Subhead' 
                        theme_text_color: 'Primary' 
                        #color: app.data.text_color 
                        text: '' 
                        halign: 'center'
                    MDLabel: 
                        id: horainicioTextField1
                        font_style: 'Subhead' 
                        theme_text_color: 'Primary' 
                        #color: app.data.text_color 
                        text: '' 
                        halign: 'center'
    #----------------------------------------------------------------------------------------------------------------
    #RESUMEN DE VIAJE USUARIO
            Screen:
                name: 'resumen_usuario'
                Widget: 
                    id: title_line 

                    canvas: 
                        Color: 
                            rgba: app.theme_cls.primary_color 
                        Rectangle: 
                            size: self.size 
                            pos: self.pos 

                    size_hint_y: None 
                    height: root.height * 30 / 100  # 30% от высоты экрана 
                    pos: 0, root.height - self.size[1] 

                Toolbar:
                    id: toolbar
                    title: 'Informe de Servicio'
                    md_bg_color: app.theme_cls.primary_color
                    background_palette: 'Primary'
                    background_hue: '500'
                    #left_action_items: [['arrow-left', lambda x: app.navigate_to_screen('home_usuario')]]
                    #right_action_items: [['dots-vertical', lambda x: app.root.toggle_nav_drawer()]]
                    pos: 0, root.height - self.size[1] 

               # Widget: 
                #    canvas: 
                 #       Ellipse: 
                  #          pos: self.pos 
                   #         size: 125, 125 
                    #        source: 'assets/user.png' 
                    #pos: (root.width / 2) - 60, root.height * 61 / 100 

                Widget: 
                    canvas: 
                        Ellipse: 
                            pos: self.pos 
                            size: 125, 125 
                            source: 'assets/check.png' 
                    pos: (root.width / 2) - 60, root.height * 61 / 100

                BoxLayout: 
                    orientation: 'vertical' 
                    size_hint: 1, None
                    #height: 50
                    spacing: 27
                    pos: self.pos[0], root.height * 20 / 130
                    MDLabel: 
                        id: info_servicio_finalizado
                        font_style: 'Title' 
                        theme_text_color: 'Primary' 
                        #color: app.data.text_color 
                        text: 'Tu Asistencia ha finalizado' 
                        halign: 'center' 
                    MDLabel: 
                        id: nombreasistenteTextField2 
                        font_style: 'Subhead' 
                        theme_text_color: 'Primary' 
                        #color: app.data.text_color 
                        text: '' 
                        halign: 'center'    
                    MDLabel: 
                        id: telefonoasistenteTextField2 
                        font_style: 'Subhead' 
                        theme_text_color: 'Primary' 
                        #color: app.data.text_color 
                        text: '' 
                        halign: 'center'  
                    MDLabel: 
                        id: dpiTextField2
                        font_style: 'Subhead' 
                        theme_text_color: 'Primary' 
                        #color: app.data.text_color 
                        text: '' 
                        halign: 'center'
                    MDLabel: 
                        id: ubicacionTextField2
                        font_style: 'Subhead' 
                        theme_text_color: 'Primary' 
                        #color: app.data.text_color 
                        text: '' 
                        halign: 'center'
                    MDLabel: 
                        id: horainicioTextField2
                        font_style: 'Subhead' 
                        theme_text_color: 'Primary' 
                        #color: app.data.text_color 
                        text: '' 
                        halign: 'center'
                    MDLabel: 
                        id: horafinTextField2
                        font_style: 'Subhead' 
                        theme_text_color: 'Primary' 
                        #color: app.data.text_color 
                        text: '' 
                        halign: 'center'
                    MDLabel: 
                        id: carryPointsTextField2
                        font_style: 'Subhead' 
                        theme_text_color: 'Primary' 
                        #color: app.data.text_color 
                        text: '' 
                        halign: 'center'
                    MDLabel: 
                        id: space_blank
                        font_style: 'Subhead' 
                        theme_text_color: 'Primary' 
                        #color: app.data.text_color 
                        text: '' 
                        halign: 'center' 
                    MDLabel: 
                        id: montoTextField2
                        font_style: 'Subhead' 
                        theme_text_color: 'Primary' 
                        #color: app.data.text_color 
                        text: '' 
                        halign: 'center'
                FloatLayout:
                    MDRaisedButton:
                        text: "Entendido / Aceptar"
                        size_hint: None, None
                        size: root.width, dp(48)
                        #pos_hint: {'center_x': 0.5, 'center_y': 0.4}
                        pos_hint_x: 0.5
                        pos: 0, 0
                        md_bg_color: get_color_from_hex(colors['BlueGrey']['500'])
                        on_release: app.regresar_home_usuario()
        
                
    #------------------------------------------------------------------------------------------------------------------


        
            Screen:
                name: 'home_asistente'
                on_enter: app.cargar_asistencias()
                BoxLayout:
                    orientation: 'vertical'

                    Toolbar:
                        id: toolbar
                        title: 'Carry U App'
                        md_bg_color: app.theme_cls.primary_color
                        background_palette: 'Primary'
                        background_hue: '500'
                        left_action_items: [['menu', lambda x: app.root.toggle_nav_drawer()]]
                        right_action_items: [['dots-vertical', lambda x: app.root.toggle_nav_drawer()]]

                    Toolbar:
                        title: "Asistencias"
                        pos_hint_x: 0.5
                        md_bg_color: get_color_from_hex(colors['BlueGrey']['700'])
                        background_palette: 'BlueGrey'
                        background_hue: '700'
                        right_action_items: [['update', lambda x:app.cargar_asistencias()]]
                        #,['arrow-left', lambda x: app.load_messagesPress()],['arrow-right', lambda x: app.load_messagesKv()]]
                    ScrollView:
                        do_scroll_x: False
                        MDList:
                            id: asistencias_mdlist
                            #TwoLineListItem:
                            #    text: "Sample text"
                            #    secondary_text: "Long message"
                            #    on_press: app.show_dialog('Sample test','Long message...')
                    #MDRaisedButton:
                        #text: "Update"
                        #size_hint: None, None
                        #size: 3 * dp(48), dp(48)
                        #pos_hint: {'center_x': 0.5, 'center_y': 0.7}
                        #pos_hint_x: 0.5
                        #opposite_colors: True
                        #on_release: app.load_messages()

            Screen:
                name: 'detalle_asistencia_usuario'
                on_enter: app.cargar_asistencia_seleccionada('info_confirmacion')
                Widget: 
                    id: title_line 

                    canvas: 
                        Color: 
                            rgba: app.theme_cls.primary_color 
                        Rectangle: 
                            size: self.size 
                            pos: self.pos 

                    size_hint_y: None 
                    height: root.height * 30 / 100  # 30% от высоты экрана 
                    pos: 0, root.height - self.size[1] 

                Toolbar:
                    id: toolbar
                    title: 'Asistencia'
                    md_bg_color: app.theme_cls.primary_color
                    background_palette: 'Primary'
                    background_hue: '500'
                    left_action_items: [['arrow-left', lambda x: app.navigate_to_screen('home_asistente')]]
                    #right_action_items: [['dots-vertical', lambda x: app.root.toggle_nav_drawer()]]
                    pos: 0, root.height - self.size[1] 

                Widget: 
                    canvas: 
                        Ellipse: 
                            pos: self.pos 
                            size: 125, 125 
                            source: 'assets/user.png' 
                    pos: (root.width / 2) - 60, root.height * 61 / 100 

                BoxLayout: 
                    orientation: 'vertical' 
                    size_hint: 1, None 
                    #height: 50
                    spacing: 25
                    pos: self.pos[0], root.height * 35 / 100 

                    MDLabel: 
                        id: nombreTextField 
                        font_style: 'Subhead' 
                        theme_text_color: 'Primary' 
                        #color: app.data.text_color 
                        text: ''
                        halign: 'center' 
                    MDLabel: 
                        id: usuarioTextField 
                        font_style: 'Subhead' 
                        theme_text_color: 'Primary' 
                        #color: app.data.text_color 
                        text: ''
                        halign: 'center' 
                    MDLabel: 
                        id: numeroTextField 
                        font_style: 'Subhead' 
                        theme_text_color: 'Primary' 
                        #color: app.data.text_color 
                        text: '' 
                        halign: 'center' 
                    MDLabel: 
                        id: ubicacionTextField 
                        font_style: 'Subhead' 
                        theme_text_color: 'Primary' 
                        #color: app.data.text_color 
                        text: '' 
                        halign: 'center'                

                MDFloatingActionButton: 
                    id: phone_end 
                    icon: 'phone' 
                    
                    size_hint: None, None 
                    size: dp(56), dp(56) 
                    #opposite_colors: True  # 
                    md_bg_color: get_color_from_hex(colors['Green']['500'])
                    elevation_normal: 8  # длинна тени 
                    pos_hint: {'center_x': .5, 'center_y': .2} 
                    on_release: app.llamarNumero()

                FloatLayout:
                    MDRaisedButton:
                        text: "Confirmar"
                        size_hint: None, None
                        size: root.width, dp(48)
                        #pos_hint: {'center_x': 0.5, 'center_y': 0.4}
                        pos_hint_x: 0.5
                        pos: 0, 0
                        md_bg_color: get_color_from_hex(colors['BlueGrey']['500'])
                        on_release: app.confirmar_asistencia()


            Screen:
                name: 'confirmacion_llegada_asistente'
                Toolbar:
                    id: toolbar
                    title: 'Confirmacion'
                    md_bg_color: app.theme_cls.primary_color
                    background_palette: 'Primary'
                    background_hue: '500'
                    left_action_items: [['arrow-left', lambda x: app.navigate_to_screen('detalle_asistencia_usuario')]]
                    #right_action_items: [['dots-vertical', lambda x: app.root.toggle_nav_drawer()]]
                    pos: 0, root.height - self.size[1] 

                BoxLayout:
                    orientation: 'vertical' 
                    size_hint: 1, None 
                    #height: 50
                    spacing: 25
                    pos: self.pos[0], root.height * 40 / 100 

                    MDLabel: 
                        id: aviso 
                        font_style: 'Subhead' 
                        theme_text_color: 'Primary' 
                        #color: app.data.text_color 
                        text: 'Confirma cuando hayas llegado con el usuario...' 
                        halign: 'center' 

                    MDSpinner:
                        id: spinner1
                        size_hint: None, None
                        size: dp(46), dp(46)
                        pos_hint: {'center_x': 0.5, 'center_y': 0.5}
                        active: True
                
                FloatLayout:
                    MDRaisedButton:
                        text: "¡Ya llegue al lugar!"
                        size_hint: None, None
                        size: root.width, dp(48)
                        #pos_hint: {'center_x': 0.5, 'center_y': 0.4}
                        pos_hint_x: 0.5
                        pos: 0, 0
                        md_bg_color: get_color_from_hex(colors['Green']['500'])
                        on_release: app.confirmar_llegada_asistente()

            


            Screen:
                name: 'servicio_en_curso_asistente'
                on_enter: app.cargar_asistencia_seleccionada('servicio_activo')
                Widget: 
                    id: title_line 

                    canvas: 
                        Color: 
                            rgba: app.theme_cls.primary_color 
                        Rectangle: 
                            size: self.size 
                            pos: self.pos 

                    size_hint_y: None 
                    height: root.height * 30 / 100  # 30% от высоты экрана 
                    pos: 0, root.height - self.size[1] 

                Toolbar:
                    id: toolbar
                    title: 'En Curso'
                    md_bg_color: app.theme_cls.primary_color
                    background_palette: 'Primary'
                    background_hue: '500'
                    left_action_items: [['arrow-left', lambda x: app.navigate_to_screen('confirmacion_llegada_asistente')]]
                    #right_action_items: [['dots-vertical', lambda x: app.root.toggle_nav_drawer()]]
                    pos: 0, root.height - self.size[1] 

                Widget: 
                    canvas: 
                        Ellipse: 
                            pos: self.pos 
                            size: 125, 125 
                            source: 'assets/wait.png' 
                    pos: (root.width / 2) - 60, root.height * 61 / 100 

                BoxLayout: 
                    orientation: 'vertical' 
                    size_hint: 1, None 
                    #height: 50
                    spacing: 25
                    pos: self.pos[0], root.height * 35 / 100 

                    MDLabel: 
                        id: tituloServicio 
                        font_style: 'Headline' 
                        theme_text_color: 'Primary' 
                        #color: app.data.text_color 
                        text: 'Tu servicio esta activo'
                        halign: 'center' 
                    MDLabel: 
                        font_style: 'Subhead' 
                        theme_text_color: 'Primary' 
                        #color: app.data.text_color 
                        text: 'Estas llevando a: '
                        halign: 'center' 
                    MDLabel: 
                        id: horaInicioLabel 
                        font_style: 'Subhead' 
                        theme_text_color: 'Primary' 
                        #color: app.data.text_color 
                        text: ''
                        halign: 'center' 
                    MDLabel: 
                        id: nombreLabel
                        font_style: 'Subhead' 
                        theme_text_color: 'Primary' 
                        #color: app.data.text_color 
                        text: ''
                        halign: 'center' 
                    MDLabel: 
                        id: numeroLabel
                        font_style: 'Subhead' 
                        theme_text_color: 'Primary' 
                        #color: app.data.text_color 
                        text: '' 
                        halign: 'center' 
                    MDLabel: 
                        id: ubicacionLabel
                        font_style: 'Subhead' 
                        theme_text_color: 'Primary' 
                        #color: app.data.text_color 
                        text: '' 
                        halign: 'center'                


                FloatLayout:
                    MDRaisedButton:
                        text: "Finalizar Servicio"
                        size_hint: None, None
                        size: root.width, dp(48)
                        #pos_hint: {'center_x': 0.5, 'center_y': 0.4}
                        pos_hint_x: 0.5
                        pos: 0, 0
                        md_bg_color: get_color_from_hex(colors['BlueGrey']['500'])
                        on_release: app.navigate_to_screen('servicio_terminado_asistente')


            Screen:
                name: 'servicio_terminado_asistente'
                on_enter: app.cargar_asistencia_seleccionada('informe_servicio_asistente')
                Widget: 
                    id: title_line 

                    canvas: 
                        Color: 
                            rgba: app.theme_cls.primary_color 
                        Rectangle: 
                            size: self.size 
                            pos: self.pos 

                    size_hint_y: None 
                    height: root.height * 30 / 100  # 30% от высоты экрана 
                    pos: 0, root.height - self.size[1] 

                Toolbar:
                    id: toolbar
                    title: 'Fin Servicio'
                    md_bg_color: app.theme_cls.primary_color
                    background_palette: 'Primary'
                    background_hue: '500'
                    left_action_items: [['arrow-left', lambda x: app.navigate_to_screen('servicio_en_curso_asistente')]]
                    #right_action_items: [['dots-vertical', lambda x: app.root.toggle_nav_drawer()]]
                    pos: 0, root.height - self.size[1] 

                Widget: 
                    canvas: 
                        Ellipse: 
                            pos: self.pos 
                            size: 125, 125 
                            source: 'assets/check.png' 
                    pos: (root.width / 2) - 60, root.height * 61 / 100 

                BoxLayout: 
                    orientation: 'vertical' 
                    size_hint: 1, None 
                    #height: 50
                    spacing: 25
                    pos: self.pos[0], root.height * 35 / 100 

                    ScrollView:
                        BoxLayout:
                            orientation: 'vertical'
                            spacing: 8
                            MDLabel: 
                                id: horaFinLabel2 
                                font_style: 'Body1' 
                                theme_text_color: 'Primary' 
                                #color: app.data.text_color 
                                text: 'Hora Fin: '
                                halign: 'center' 
                            MDLabel: 
                                id: nombreLabel2
                                font_style: 'Body1' 
                                theme_text_color: 'Primary' 
                                #color: app.data.text_color 
                                text: 'Nombre: '
                                halign: 'center' 
                            MDLabel: 
                                id: ubicacionLabel2
                                font_style: 'Body1' 
                                theme_text_color: 'Primary' 
                                #color: app.data.text_color 
                                text: 'Lugar: ' 
                                halign: 'center' 
                            MDLabel: 
                                id: duracionLabel2
                                font_style: 'Body1' 
                                theme_text_color: 'Primary' 
                                #color: app.data.text_color 
                                text: 'Duracion: ' 
                                halign: 'center' 

                            BoxLayout:
                                orientation: 'horizontal'
                                pos_hint: {'center_x': 0.5, 'center_y': 0.4} 
                                #spacing: 8
                                MDLabel: 
                                    id: puntosLabel
                                    font_style: 'Body1' 
                                    theme_text_color: 'Primary' 
                                    #color: app.data.text_color 
                                    text: 'Puntos: '  
                                    halign: 'center'                       
                                MDCheckbox:
                                    id:           checkPuntos
                                    group:        'pago'
                                    #size_hint:    None, None
                                    size:        dp(30), dp(30)

                            BoxLayout:
                                orientation: 'horizontal'
                                #spacing: 8
                                pos_hint: {'center_x': 0.5, 'center_y': 0.4} 
                                MDLabel: 
                                    id: dineroLabel
                                    font_style: 'Body1' 
                                    theme_text_color: 'Primary' 
                                    #color: app.data.text_color 
                                    text: 'Dinero: '       
                                    halign: 'center'                  
                                MDCheckbox:
                                    id:           checkMonto
                                    group:        'pago'
                                    #size_hint:    None, None
                                    size:        dp(30), dp(30)


                FloatLayout:
                    MDRaisedButton:
                        text: "Canjear/Cobrar"
                        size_hint: None, None
                        size: root.width, dp(48)
                        #pos_hint: {'center_x': 0.5, 'center_y': 0.4}
                        pos_hint_x: 0.5
                        pos: 0, 0
                        md_bg_color: get_color_from_hex(colors['BlueGrey']['500'])
                        on_release: app.terminar_asistencia()



'''


class HackedDemoNavDrawer(MDNavigationDrawer):
    # DO NOT USE
    def add_widget(self, widget, index=0):
        if issubclass(widget.__class__, BaseListItem):
            self._list.add_widget(widget, index)
            if len(self._list.children) == 1:
                widget._active = True
                self.active_item = widget
            # widget.bind(on_release=lambda x: self.panel.toggle_state())
            widget.bind(on_release=lambda x: x._set_active(True, list=self))
        elif issubclass(widget.__class__, NavigationDrawerHeaderBase):
            self._header_container.add_widget(widget)
        else:
            super(MDNavigationDrawer, self).add_widget(widget, index)


class KitchenSink(App):
    theme_cls = ThemeManager()
    previous_date = ObjectProperty()
    title = "Carry U App"
    asistencia = {}
    usuarioLogueado = ""
    rol = 0

    menu_items = [
        {'viewclass': 'MDMenuItem',
         'text': 'Example item'},
        {'viewclass': 'MDMenuItem',
         'text': 'Example item'},
        {'viewclass': 'MDMenuItem',
         'text': 'Example item'},
        {'viewclass': 'MDMenuItem',
         'text': 'Example item'},
        {'viewclass': 'MDMenuItem',
         'text': 'Example item'},
        {'viewclass': 'MDMenuItem',
         'text': 'Example item'},
        {'viewclass': 'MDMenuItem',
         'text': 'Example item'},
    ]

    def build(self):
        main_widget = Builder.load_string(main_widget_kv)
        #self.theme_cls.theme_style = 'Dark'

        #main_widget.ids.text_field_error.bind(
        #    on_text_validate=self.set_error_message,
        #    on_focus=self.set_error_message)
        self.bottom_navigation_remove_mobile(main_widget)
        Window.borderless = True
        return main_widget

    def bottom_navigation_remove_mobile(self, widget):
        # Removes some items from bottom-navigation demo when on mobile
        if DEVICE_TYPE == 'mobile':
            widget.ids.bottom_navigation_demo.remove_widget(widget.ids.bottom_navigation_desktop_2)
        if DEVICE_TYPE == 'mobile' or DEVICE_TYPE == 'tablet':
            widget.ids.bottom_navigation_demo.remove_widget(widget.ids.bottom_navigation_desktop_1)


    #Tratar de loguearme
    def loguearse(self):
        usuario= self.root.ids.usuarioTextfield.text
        contrasena= self.root.ids.passTextfield.text
        r = login(db, usuario, contrasena)      
        if r==1:
            self.show_example_snackbar("simple", "Fallido")
        elif r==2:
            self.usuarioLogueado = usuario
            self.rol = 1
            self.show_example_snackbar("simple", "Es usuario")
            self.root.ids.scr_mngr.current = 'home_usuario'
        elif r==3:
            self.usuarioLogueado = usuario
            self.rol = 2
            self.show_example_snackbar("simple", "Es asistente")
            self.root.ids.scr_mngr.current = 'home_asistente'
        

    def navigate_to_screen(self, name):
        self.root.ids.scr_mngr.current = name



    def registrarse(self, tipo):
        if(tipo == "usuario"): #Se registra como usuario
            nombre = self.root.ids.nombreTextField.text
            apellido = self.root.ids.apellidoTextField.text
            usuario = self.root.ids.usuarioTextField.text
            contrasena = self.root.ids.contrasenaTextField.text
            #CheckBoxes
            sexo = "M"
            if(self.root.ids.checkSexoF.active == True and self.root.ids.checkSexoM.active == False):
                sexo = "F"                        
            #Fechas           
            fechaDeNacimiento = self.root.ids.date_picker_label.text
            dpi = self.root.ids.dpiTextField.text
            telefono = self.root.ids.numeroTextField.text
            correo = self.root.ids.correoTextField.text
            #Switches
            cargar = "N"
            if(self.root.ids.switchCargar.active):
                cargar = "S"                
            movilizar = "N"
            if(self.root.ids.switchMovilizar.active):
                movilizar = "S"
            silla = "N"
            if(self.root.ids.switchLlevarSilla.active):
                silla = "S"

            usuarioNuevo={"nombre":nombre,
                    "apellido":apellido,
                    "usuario":usuario,
                    "contrasena":contrasena,
                    "fechanacimiento":fechaDeNacimiento,
                    "DPI":dpi,                 
                    "telefono":telefono,
                    "correo":correo,
                    "sexo":sexo,
                    "aptitudCargar":cargar,
                    "aptidudMovilizar":movilizar,
                    "aptitudMoverSilla":silla}
        
            if(registrarUsuario(db, usuarioNuevo, 1)):
                self.show_example_snackbar("simple", "Felicidades, te has registrado como Usuario")
                self.root.ids.scr_mngr.current = 'login'
            else:
                self.show_example_snackbar("simple", "Error en el registro, intenta de nuevo...")
        
        elif(tipo == "asistente"): #Esta vez se trata de registrar como asistente
            nombre = self.root.ids.nombreTextField.text
            apellido = self.root.ids.apellidoTextField.text
            usuario = self.root.ids.usuarioTextField.text
            contrasena = self.root.ids.contrasenaTextField.text
            #CheckBoxes
            sexo = "M"
            if(self.root.ids.checkSexoF.active == True and self.root.ids.checkSexoM.active == False):
                sexo = "F"                        
            #Fechas           
            fechaDeNacimiento = self.root.ids.date_picker_label.text
            dpi = self.root.ids.dpiTextField.text
            telefono = self.root.ids.numeroTextField.text
            correo = self.root.ids.correoTextField.text
            #Switches
            cargar = "N"
            if(self.root.ids.switchCargar.active):
                cargar = "S"                
            movilizar = "N"
            if(self.root.ids.switchMovilizar.active):
                movilizar = "S"
            silla = "N"
            if(self.root.ids.switchLlevarSilla.active):
                silla = "S"

            asistenteNuevo={"nombre":nombre,
                    "apellido":apellido,
                    "usuario":usuario,
                    "contrasena":contrasena,
                    "fechanacimiento":fechaDeNacimiento,
                    "DPI":dpi,                 
                    "telefono":telefono,
                    "correo":correo,
                    "sexo":sexo,
                    "aptitudCargar":cargar,
                    "aptidudMovilizar":movilizar,
                    "aptitudMoverSilla":silla}
        
            if(registrarUsuario(db, asistenteNuevo, 2)):
                self.show_example_snackbar("simple", "Felicidades, te has registrado como Asistente")
                self.root.ids.scr_mngr.current = 'login'
            else:
                self.show_example_snackbar("simple", "Error en el registro, intenta de nuevo...")


    def cargar_asistencias(self):
        self.root.ids.asistencias_mdlist.clear_widgets()
        asistenciasPendientes = buscarAsistenciasPorEstado(db, "En espera")
        if(asistenciasPendientes.count() > 0):
            index = 0
            #app = App.get_running_app()
            for asistencia in asistenciasPendientes:

                item = TwoLineAvatarIconListItem(
                    text=asistencia["ubicacion"],
                    secondary_text=asistencia["usuario"]
                )
                #item.add_widget(ContactPhoto(source=""))
                item.add_widget(self.MessageButton(idAsistencia=str(asistencia["_id"])))
                self.root.ids.asistencias_mdlist.add_widget(item)

                '''tlli = TwoLineListItem(
                            id= str(index + 1),
                            text= asistencia["ubicacion"],
                            secondary_text= asistencia["usuario"],
                            on_press= lambda x:self.confirmar_asistencia(index))
                
                item = tlli# Builder.load_string(tlli)
                self.root.ids.asistencias_mdlist.add_widget(item)
                index += 1'''
        else:
            print("Lista vacia men :(")

    '''def get_id(self,  instance):
        for id, widget in instance.ids.items():
            if widget.__self__ == instance:
                return id
    '''

    def solicitar_asistencia(self): #vOY A MODIFICAR HORA INICIO
        horaFin = strftime("%H:%M:%S")
        nombre = self.root.ids.usuarioTextfield.text
        place = self.root.ids.lugar_asistenciaTextField.text
        asistencia = {"asistente":"",
                      "usuario":nombre,
                      "ubicacion":place,
                      "estado": "En espera",
                      "fecha": strftime("%d-%m-%Y"),
                      "horaInicio": "", #strftime("%H:%M:%S"),
                      "horaFin":"",
                      "carryPoints":0,
                      "monto":0}                        
        if(agregarAsistencia(db, asistencia)):
            self.root.ids.scr_mngr.current = 'en_espera_usuario'

    def Search_Asistencia(self):
        if(buscarAsistencia(db, 0, self.usuarioLogueado, self.rol).count() > 0):
            self.asistencia = buscarAsistencia(db, 0, self.usuarioLogueado, self.rol)[0]

            while(verificarEstadoAsistencia(db, self.asistencia["_id"], self.usuarioLogueado, self.rol) == "En espera"):
                sleep(4)
                #Se asume que el estado de la asistencia cambio a "Confirmada"
                #asistencia = buscarAsistencia(db, asistencia["_id"], self.usuarioLogueado)
            
            self.asistencia = buscarAsistencia(db, self.asistencia["_id"], self.usuarioLogueado, self.rol)

            if(self.asistencia["estado"] == "Confirmada"):
                asistente = obtenerAsistente(db, self.asistencia["asistente"])
                self.root.ids.nombreasistenteTextField.text = str("Tu Asistente es: "+ asistente["nombre"] + " " + asistente["apellido"])
                self.root.ids.telefonoasistenteTextField.text = str("Número de teléfono: "+ asistente["telefono"])
                self.root.ids.dpiTextField.text = str("DPI: "+ asistente["DPI"])
                if asistente["sexo"]=="M":
                    self.root.ids.sexoTextField.text = "Sexo: Masculino"
                else:
                    self.root.ids.sexoTextField.text = "Sexo: Femenino"
                if asistente["aptitudCargar"]=="S":
                    self.root.ids.aptitud_CargarTextField.text = "Capacidad de cargar cosas: SI"
                else:
                    self.root.ids.aptitud_CargarTextField.text = "Capacidad de cargar cosas: NO"
                if asistente["aptidudMovilizar"]=="S":
                    self.root.ids.aptitud_MovilizarTextField.text = "Capacidad de ayudar a Movilizarte: SI"
                else:
                    self.root.ids.aptitud_MovilizarTextField.text = "Capacidad de ayudar a Movilizarte: NO"
                if asistente["aptitudMoverSilla"]=="S":
                    self.root.ids.aptitud_MoverSillaTextField.text = "Capacidad de ayudar con silla/ muletas: SI"
                else:
                    self.root.ids.aptitud_MoverSillaTextField.text = "Capacidad de ayudar con silla/ muletas: NO"
                self.root.ids.scr_mngr.current = 'detalle_asistente_usuario'
    
    def en_espera_llegada(self):
        print(self.asistencia)
        while(buscarAsistencia(db, self.asistencia["_id"], self.usuarioLogueado, self.rol)["horaInicio"] == ""):
            sleep(4)
                        #Aqui ya llego el asistente, y cambio la horaInicio #Asistente: Ya llegue a las 5, 
        
        self.asistencia = buscarAsistencia(db, self.asistencia["_id"], self.usuarioLogueado, self.rol)
        asistente = obtenerAsistente(db, self.asistencia["asistente"])
        self.root.ids.nombreasistenteTextField1.text = str("Tu Asistente es: "+ asistente["nombre"] + " " + asistente["apellido"])
        self.root.ids.telefonoasistenteTextField1.text = str("Número de teléfono: "+ asistente["telefono"])
        self.root.ids.dpiTextField1.text = str("DPI: "+ asistente["DPI"])
        self.root.ids.ubicacionTextField1.text = str("Lugar de la asistencia: "+ self.asistencia["ubicacion"])
        self.root.ids.horainicioTextField1.text = str("Hora de Inicio del servicio: "+ self.asistencia["horaInicio"])
        self.root.ids.scr_mngr.current = 'en_curso_usuario' 
    def espera_finalizacion(self):
        while(verificarEstadoAsistencia(db, self.asistencia["_id"], self.usuarioLogueado, self.rol) == "En curso"):
            sleep(4)
        self.asistencia = buscarAsistencia(db, self.asistencia["_id"], self.usuarioLogueado, self.rol)               #Hasta aqui, ya la asistencia ha terminado, entonces el estado cambia a "Finalizado"
        asistente = obtenerAsistente(db, self.asistencia["asistente"])
        self.root.ids.nombreasistenteTextField2.text = str("Fuiste Asistido por: "+ asistente["nombre"] + " " + asistente["apellido"])
        self.root.ids.telefonoasistenteTextField2.text = str("Número de teléfono: "+ asistente["telefono"])
        self.root.ids.dpiTextField2.text = str("DPI: "+ asistente["DPI"])
        self.root.ids.ubicacionTextField2.text = str("Lugar de la asistencia: "+ self.asistencia["ubicacion"])
        self.root.ids.horainicioTextField2.text = str("Hora de Inicio del servicio: "+ self.asistencia["horaInicio"])
        self.root.ids.horafinTextField2.text = str("Hora de Inicio del servicio: "+ self.asistencia["horaFin"])
        #if self.asistencia["carryPoints"]!=0:
         #   self.root.ids.carrypointsTextField.text = str("El Asistente decidió cobrar con "+ str(self.asistencia["carryPoints"])+ " CarryPoints como recompensa" )
        self.root.ids.scr_mngr.current = 'resumen_usuario' 
        if self.asistencia["carryPoints"]!=0:
            self.root.ids.carryPointsTextField2.text = str("El asistente ha escogido recibir  "+ str("{0:.2f}".format(self.asistencia["carryPoints"]))+" como recompensa")
        elif self.asistencia["monto"]!=0:
            self.root.ids.montoTextField2.text = str("El asistente ha escogido recibir  "+ str("{0:.2f}".format(self.asistencia["monto"]))+" como recompensa")


    def regresar_home_usuario(self):
        self.root.ids.scr_mngr.current = 'home_usuario' 

    
    #--------------------------------------------------------------------------------------------------------------        

    def seleccionar_asistencia(self, idAsistencia):
        #print(self.get_id(instance))
        self.asistencia = buscarAsistencia(db, ObjectId(idAsistencia), self.usuarioLogueado, self.rol)
        #print(self.asistencia)
        self.root.ids.scr_mngr.current = 'detalle_asistencia_usuario' 

    def cargar_asistencia_seleccionada(self, pantalla):
        usuario = obtenerUsuario(db , self.asistencia["usuario"])
        if(pantalla == "info_confirmacion"):
            self.root.ids.nombreTextField.text = str(usuario["nombre"] + " " + usuario["apellido"])
            self.root.ids.usuarioTextField.text = str(self.asistencia["usuario"])
            self.root.ids.numeroTextField.text = str(usuario["telefono"])
            self.root.ids.ubicacionTextField.text = str(self.asistencia["ubicacion"])
        elif(pantalla == "servicio_activo"):
            self.root.ids.nombreLabel.text = str(usuario["nombre"] + " " + usuario["apellido"])
            #self.root.ids.usuarioLabel.text = str(self.asistencia["usuario"])
            self.root.ids.numeroLabel.text = str(usuario["telefono"])
            self.root.ids.ubicacionLabel.text = str(self.asistencia["ubicacion"])
            self.root.ids.horaInicioLabel.text = str(self.asistencia["horaInicio"])
        elif(pantalla == "informe_servicio_asistente"):
            carryPoints = 200 #Por hora                                            
            monto = 40 #Por hora
            horaFin = strftime("%H:%M:%S")
            horasTrabajadas = calcularHorasTrabajadas(self.asistencia["fecha"], self.asistencia["horaInicio"], horaFin)
            self.root.ids.horaFinLabel2.text += str(horaFin)
            self.root.ids.nombreLabel2.text += str(usuario["nombre"] + " " + usuario["apellido"])
            self.root.ids.ubicacionLabel2.text += str(self.asistencia["ubicacion"])
            self.root.ids.duracionLabel2.text += str("{0:.2f}".format(round(horasTrabajadas,2))) + " h."
            self.root.ids.puntosLabel.text += str("{0:.2f}".format(round(horasTrabajadas * carryPoints,2)))
            self.root.ids.dineroLabel.text += str("{0:.2f}".format(round(horasTrabajadas * monto,2)))
            

    def llamarNumero(self):
        number =  self.root.ids.numeroTextField.text
        print("callNum(): " + number)

        '''PythonActivity = autoclass('org.renpy.android.PythonActivity')
        Intent = autoclass('android.content.Intent')
        Uri = autoclass('android.net.Uri')

        intent = Intent(Intent.ACTION_CALL)
        intent.setData(Uri.parse("tel:" + number))
        currentActivity = cast('android.app.Activity', PythonActivity.mActivity)
        currentActivity.startActivity(intent)'''

    def confirmar_asistencia(self):
        cambios = {"asistente": self.usuarioLogueado, "estado": "Confirmada"}
        if(modificarAsistencia(db, cambios, self.asistencia["_id"], self.usuarioLogueado, self.rol)):
            self.root.ids.scr_mngr.current = 'confirmacion_llegada_asistente'

    def confirmar_llegada_asistente(self):
        horaInicio = strftime("%H:%M:%S")
        cambios = {"estado": "En curso", "horaInicio": horaInicio}
        if(modificarAsistencia(db, cambios, self.asistencia["_id"], self.usuarioLogueado, self.rol)):
            self.asistencia = buscarAsistencia(db, self.asistencia["_id"], self.usuarioLogueado, self.rol)
            self.root.ids.scr_mngr.current = 'servicio_en_curso_asistente'

    def terminar_asistencia(self):
        print("Fin de asistencia, iniciando muestra de reporte...")
        carryPoints = 0
        monto = 0
        if(self.root.ids.checkPuntos.active == True and self.root.ids.checkMonto.active == False):
            carryPoints = 200 #Por hora   
        elif(self.root.ids.checkPuntos.active != True and self.root.ids.checkMonto.active != False):
            monto = 40 #Por hora
        horaFin = strftime("%H:%M:%S")
        horasTrabajadas = calcularHorasTrabajadas(self.asistencia["fecha"], self.asistencia["horaInicio"], horaFin)
        cambios = {"estado": "Finalizado", "horaFin": horaFin, "carryPoints": carryPoints * horasTrabajadas, "monto": monto * horasTrabajadas}
        if(modificarAsistencia(db, cambios, self.asistencia["_id"], self.usuarioLogueado, self.rol)):
            self.asistencia = buscarAsistencia(db, self.asistencia["_id"], self.usuarioLogueado, self.rol) 
            self.show_example_snackbar("simple", "Asistencia terminada...")
            self.root.ids.scr_mngr.current = 'home_asistente'





    
    class MessageButton(IRightBodyTouch, MDIconButton): #Inner class
        idAsistencia = StringProperty()
        
        def on_release(self):
            # sample code:
            #Dialer.send_sms(phone_number, "Hey! What's up?")
            app = App.get_running_app()
            app.seleccionar_asistencia(self.idAsistencia)
            pass







        


    def show_example_snackbar(self, snack_type, text):
        if snack_type == 'simple':
            Snackbar(text=text).show()
        elif snack_type == 'button':
            Snackbar(text=text, button_text="with a button!", button_callback=lambda *args: 2).show()
        elif snack_type == 'verylong':
            Snackbar(text=text).show()

    def show_example_dialog(self):
        content = MDLabel(font_style='Body1',
                          theme_text_color='Secondary',
                          text="This is a dialog with a title and some text. "
                               "That's pretty awesome right!",
                          size_hint_y=None,
                          valign='top')
        content.bind(texture_size=content.setter('size'))
        self.dialog = MDDialog(title="This is a test dialog",
                               content=content,
                               size_hint=(.8, None),
                               height=dp(200),
                               auto_dismiss=False)

        self.dialog.add_action_button("Dismiss",
                                      action=lambda *x: self.dialog.dismiss())
        self.dialog.open()

    def show_example_long_dialog(self):
        content = MDLabel(font_style='Body1',
                          theme_text_color='Secondary',
                          text="Lorem ipsum dolor sit amet, consectetur "
                               "adipiscing elit, sed do eiusmod tempor "
                               "incididunt ut labore et dolore magna aliqua. "
                               "Ut enim ad minim veniam, quis nostrud "
                               "exercitation ullamco laboris nisi ut aliquip "
                               "ex ea commodo consequat. Duis aute irure "
                               "dolor in reprehenderit in voluptate velit "
                               "esse cillum dolore eu fugiat nulla pariatur. "
                               "Excepteur sint occaecat cupidatat non "
                               "proident, sunt in culpa qui officia deserunt "
                               "mollit anim id est laborum.",
                          size_hint_y=None,
                          valign='top')
        content.bind(texture_size=content.setter('size'))
        self.dialog = MDDialog(title="This is a long test dialog",
                               content=content,
                               size_hint=(.8, None),
                               height=dp(200),
                               auto_dismiss=False)

        self.dialog.add_action_button("Dismiss",
                                      action=lambda *x: self.dialog.dismiss())
        self.dialog.open()

    def get_time_picker_data(self, instance, time):
        self.root.ids.time_picker_label.text = str(time)
        self.previous_time = time

    def show_example_time_picker(self):
        self.time_dialog = MDTimePicker()
        self.time_dialog.bind(time=self.get_time_picker_data)
        if self.root.ids.time_picker_use_previous_time.active:
            try:
                self.time_dialog.set_time(self.previous_time)
            except AttributeError:
                pass
        self.time_dialog.open()

    def set_previous_date(self, date_obj):
        self.previous_date = date_obj
        self.root.ids.date_picker_label.text = str(date_obj)

    def show_example_date_picker(self):
        #if self.root.ids.date_picker_use_previous_date.active:
            #pd = self.previous_date
        #try:
        #    MDDatePicker(self.set_previous_date,
        #                pd.year, pd.month, pd.day).open()
        #except AttributeError:
        #    MDDatePicker(self.set_previous_date).open()
        #else:
        time = datetime(1999, 6, 1, 11, 13, 3, 57000)
        MDDatePicker(self.set_previous_date, time.year, time.month, time.day).open()

    def show_example_bottom_sheet(self):
        bs = MDListBottomSheet()
        bs.add_item("Here's an item with text only", lambda x: x)
        bs.add_item("Here's an item with an icon", lambda x: x,
                    icon='clipboard-account')
        bs.add_item("Here's another!", lambda x: x, icon='nfc')
        bs.open()

    def show_example_grid_bottom_sheet(self):
        bs = MDGridBottomSheet()
        bs.add_item("Facebook", lambda x: x,
                    icon_src='./assets/facebook-box.png')
        bs.add_item("YouTube", lambda x: x,
                    icon_src='./assets/youtube-play.png')
        bs.add_item("Twitter", lambda x: x,
                    icon_src='./assets/twitter.png')
        bs.add_item("Da Cloud", lambda x: x,
                    icon_src='./assets/cloud-upload.png')
        bs.add_item("Camera", lambda x: x,
                    icon_src='./assets/camera.png')
        bs.open()

    def set_error_message(self, *args):
        if len(self.root.ids.text_field_error.text) == 2:
            self.root.ids.text_field_error.error = True
        else:
            self.root.ids.text_field_error.error = False

    def on_pause(self):
        return True

    def on_stop(self):
        pass


class AvatarSampleWidget(ILeftBody, Image):
    pass


class IconLeftSampleWidget(ILeftBodyTouch, MDIconButton):
    pass


class IconRightSampleWidget(IRightBodyTouch, MDCheckbox):
    pass


if __name__ == '__main__':
    KitchenSink().run()
